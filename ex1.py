from copy import deepcopy
import numpy as np
from matplotlib import pyplot as plt
import load
import init_centroids
import matplotlib.image as image


def print_cent(cent):
    if type(cent) == list:
        cent = np.asarray(cent)
    if len(cent.shape) == 1:
        return ' '.join(str(np.floor(100*cent)/100).split()).replace('[ ', '[').replace('\n', ' ').replace(' ]',']').replace(' ', ', ')
    else:
        return ' '.join(str(np.floor(100*cent)/100).split()).replace('[ ', '[').replace('\n', ' ').replace(' ]',']').replace(' ', ', ')[1:-1]


def floor2d(x):
    ans = []
    for elem in x:
        ans.append(np.floor(elem * 100) / 100)
    return ans


def floor4d(x):
    return round(x, 4)


def get_centroids(k):
    k_centroids = init_centroids.init_centroids(0, k)
    return k_centroids


# Euclidean Distance Caculator
def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)


def printing(iteration, C_old):
    print("iter " + str(iteration) + ": ", end="")
    print(print_cent(C_old))
    # for c in C_old:
    #     print(str(floor2d(c)), end=", ")


def k_means(centroids, x):
    # Number of clusters
    C = centroids
    print("k=" + str(len(C)))

    # To store the value of centroids when it updates
    C_old = np.zeros(C.shape)
    # Cluster Lables(0, 1, 2)
    clusters = np.zeros(len(x))
    # Error func. - Distance between new centroids and old centroids
    error = dist(C, C_old, None)
    error_list = []
    # Loop will run till the error becomes zero
    iteration = 0
    while iteration <= 10:
        # Assigning each value to its closest cluster
        for i in range(len(x)):
            distances = dist(x[i], C)
            cluster = np.argmin(distances)
            clusters[i] = cluster
        # Storing the old centroid values
        C_old = deepcopy(C)
        # Finding the new centroids by taking the average value
        for i in range(len(C)):
            points = [x[j] for j in range(len(x)) if clusters[j] == i]
            C[i] = np.mean(points, axis=0)
        error = dist(C, C_old, None)
        error_list.append(error)
        printing(iteration, C_old)
        iteration += 1
    # new_pic = []
    # for index, val in enumerate(clusters):
    #     new_pic.append(C_old[int(val)])
    # print("avg loss: ")
    # for x in error_list:
    #     print(floor4d(x), end=", ")
    # print()
    # plt.plot(range(iteration), error_list, '-gD')
    # plt.show()
    # return new_pic


def main():
    number_of_k = [2 ** n for n in range(1, 5)]
    for k in number_of_k:
        k_centroids = get_centroids(k)
        k_means(k_centroids, load.X)
        # new_pic = k_means(k_centroids, load.X)
        # new_pic = np.reshape(new_pic, (load.img_size[0], load.img_size[1], load.img_size[2]))
        # name = "image" + str(k) + ".jpeg"
        # image.imsave(name, new_pic)


if __name__ == "__main__":
    main()
